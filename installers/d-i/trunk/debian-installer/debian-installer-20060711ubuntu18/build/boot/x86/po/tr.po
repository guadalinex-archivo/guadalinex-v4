# Turkish translation for debian-installer
# Copyright (c) (c) 2006 Canonical Ltd, and Rosetta Contributors 2006
# This file is distributed under the same license as the debian-installer package.
# FIRST AUTHOR <EMAIL@ADDRESS>, 2006.
#
#, fuzzy
msgid ""
msgstr ""
"Project-Id-Version: debian-installer\n"
"Report-Msgid-Bugs-To: FULL NAME <EMAIL@ADDRESS>\n"
"POT-Creation-Date: 2006-09-23  1:26+0100\n"
"PO-Revision-Date: 2006-06-27 10:01+0000\n"
"Last-Translator: Umut Güçlü <a0309868@unet.univie.ac.at>\n"
"Language-Team: Turkish <tr@li.org>\n"
"MIME-Version: 1.0\n"
"Content-Type: text/plain; charset=UTF-8\n"
"Content-Transfer-Encoding: 8bit\n"

#: help.xml:6
msgid "Installer Boot Help Screens"
msgstr "Önyükleme Yardım Ekranı Kuru"

#: help.xml:11
msgid "Welcome to ${DISTRIBUTION_NAME}!"
msgstr "${DISTRIBUTION_NAME}'e hoşgeldiniz!"

#: help.xml:18
msgid ""
"This is an installation ${MEDIA_TYPE} for ${DISTRIBUTION_NAME} "
"${DISTRIBUTION_VERSION}, built on ${BUILD_DATE}."
msgstr ""
"Bu ${BUILD_DATE} tarihli ${DISTRIBUTION_NAME} ${DISTRIBUTION_VERSION} için "
"bir kurulum ${MEDIA_TYPE}'dir."

#: help.xml:23
msgid ""
"This is a live ${MEDIA_TYPE} for ${DISTRIBUTION_NAME} "
"${DISTRIBUTION_VERSION}, built on ${BUILD_DATE}."
msgstr ""
"Bu ${BUILD_DATE} tarihli ${DISTRIBUTION_NAME} ${DISTRIBUTION_VERSION} için "
"bir çalışan ${MEDIA_TYPE}'dir."

#: help.xml:28
msgid "HELP INDEX"
msgstr "YARDIM DİZİNİ"

#: help.xml:29
msgid "KEY"
msgstr "ANAHTAR"

#: help.xml:30
msgid "TOPIC"
msgstr "KONU"

#: help.xml:34
msgid "This page, the help index."
msgstr "Bu sayfa, yardım dizini."

#: help.xml:39
msgid "Prerequisites for installing ${DISTRIBUTION_NAME}."
msgstr "${DISTRIBUTION_NAME}'in kurulması için önkoşullar."

#: help.xml:44
msgid "Prerequisites for running ${DISTRIBUTION_NAME}."
msgstr "${DISTRIBUTION_NAME}'in çalıştırılması için önkoşullar."

#: help.xml:49
msgid "Boot methods for special ways of using this ${MEDIA_TYPE}."
msgstr "${MEDIA_TYPE} kullanımının özel yolları için önyükleme yöntemleri."

#: help.xml:54
msgid "Additional boot methods; rescuing a broken system."
msgstr ""

#: help.xml:59
msgid "Special boot parameters, overview."
msgstr "Özel önyükleme parametreleri; toplu bakış."

#: help.xml:64
msgid "Special boot parameters for special machines."
msgstr "Özel makinalar için özel önyükleme parametreleri."

#: help.xml:69
msgid "Special boot parameters for selected disk controllers."
msgstr "Seçili disk denetleyicileri için özel önyğkleme parametreleri."

#: help.xml:74
msgid "Special boot parameters for the install system."
msgstr "Kurulum sistemi için özel önyükleme parametreleri."

#: help.xml:79
msgid "Special boot parameters for the bootstrap system."
msgstr "Önyükleyici sistem için özel önyükleme parametreleri."

#: help.xml:84
msgid "How to get help."
msgstr "Nasıl yardım alınır."

#: help.xml:89
msgid "Copyrights and warranties."
msgstr "Telif hakları ve garantiler."

#: help.xml:100
msgid "Press F2 through F10 for details, or ENTER to ${BOOTPROMPT}"
msgstr ""
"Detaylar için F2'den F10'a kadar bir tuşa veya ${BOOTPROMPT} için ENTER "
"tuşuna basın."

#: help.xml:104
msgid "Press F2 through F10 for details, or Escape to exit help."
msgstr ""
"Detaylar için F2'den F10'a kadar bir tuşa veya yardımdan çıkmak için Escape "
"tuşuna basın."

#: help.xml:112
msgid "PREREQUISITES FOR INSTALLING UBUNTU"
msgstr "UBUNTU KURULUMU İÇİN ÖNKOŞULLAR"

#: help.xml:119
msgid ""
"You must have at least 32 megabytes of RAM to use this Ubuntu installer."
msgstr "Ubuntu kuru için en az 32 megabytes RAM gereklidir."

#: help.xml:123
msgid ""
"You should have space on your hard disk to create a new disk partition of at "
"least 2 gigabytes to install a standard Ubuntu desktop system or at least "
"400 megabytes for a minimal server installation. You'll need more disk space "
"to install additional packages, depending on what you wish to do with your "
"new Ubuntu system."
msgstr ""

#: help.xml:131 help.xml:653
msgid ""
"See the Installation Manual or the FAQ for more information; both documents "
"are available at the Ubuntu web site, <ulink url=\"http://www.ubuntu.com/\" /"
">"
msgstr ""

#: help.xml:137
msgid ""
"You must have at least 128 megabytes of RAM to use this Ubuntu live system."
msgstr ""

#: help.xml:142
msgid ""
"The live system does not require any space on your hard disk. However, "
"existing swap partitions on the disk will be used if available."
msgstr ""

#: help.xml:147 help.xml:659
msgid ""
"See the FAQ for more information; this document is available at the Ubuntu "
"web site, <ulink url=\"http://www.ubuntu.com/\" />"
msgstr ""

#: help.xml:152
msgid "Thank you for choosing Ubuntu!"
msgstr ""

#: help.xml:156 help.xml:230 help.xml:266 help.xml:295 help.xml:362
#: help.xml:448 help.xml:517 help.xml:608 help.xml:664 help.xml:720
msgid ""
"Press <phrase class=\"not-serial\">F1</phrase><phrase class=\"serial"
"\">control and F then 1</phrase> for the help index, or ENTER to "
"${BOOTPROMPT}"
msgstr ""

#: help.xml:161 help.xml:235 help.xml:271 help.xml:300 help.xml:367
#: help.xml:453 help.xml:522 help.xml:613 help.xml:669 help.xml:725
msgid "Press F1 for the help index, or Escape to exit help."
msgstr ""
"Yardım dizini için F1 tuşuna veya yardımdan çıkmak için Escape tuşuna basın."

#: help.xml:169
msgid "BOOT METHODS"
msgstr "ÖNYÜKLEME YÖNTEMLERİ"

#: help.xml:173
msgid "Available boot methods:"
msgstr "Kullanılabilir önyükleme yöntemleri:"

#: help.xml:177
msgid "install"
msgstr "install"

#: help.xml:179
msgid "Start the installation -- this is the default ${MEDIA_TYPE} install."
msgstr "Kurulumu başlatır -- bu varsayılan ${MEDIA_TYPE} kurulumudur."

#: help.xml:184
msgid "expert"
msgstr "expert"

#: help.xml:186
msgid "Start the installation in expert mode, for maximum control."
msgstr "Maksimum kontrol için, kurulumu uzman kipinde başlatır."

#: help.xml:191
msgid "server"
msgstr "server"

#: help.xml:192
msgid "server-expert"
msgstr "server-expert"

#: help.xml:194
msgid ""
"Minimal system install. Recommended for servers, where a complete graphical "
"desktop environment is not desired."
msgstr ""
"Minimal sistem kurulumu. Bir çizgisel masaüstü ortamı istenmeğem sunucular "
"için önerilir."

#: help.xml:200
msgid "live"
msgstr "live"

#: help.xml:202
msgid "Start the live system."
msgstr "Çalışan sistemi başlatır."

#: help.xml:207
msgid "memtest"
msgstr "memtest"

#: help.xml:209
msgid "Perform a memory test."
msgstr "Bellek testi uygula."

#: help.xml:215 help.xml:259
msgid ""
"To use one of these boot methods, type it at the prompt, optionally followed "
"by any boot parameters. For example:"
msgstr ""

#: help.xml:220
#, no-wrap
msgid "boot: install acpi=off"
msgstr ""

#: help.xml:221
#, no-wrap
msgid "boot: live acpi=off"
msgstr ""

#: help.xml:225
msgid ""
"If unsure, you should use the default boot method, with no special "
"parameters, by simply pressing enter at the boot prompt."
msgstr ""

#: help.xml:243 help.xml:279
msgid "RESCUING A BROKEN SYSTEM"
msgstr ""

#: help.xml:247
msgid "Use one of these boot methods to rescue an existing install"
msgstr ""
"Varolan bir kurulumu kurtarmak için bu önyükleme yöntemlerinden birini "
"kullanın."

#: help.xml:251
msgid "rescue"
msgstr ""

#: help.xml:253
msgid "Boot into rescue mode."
msgstr ""

#: help.xml:263
#, no-wrap
msgid "boot: rescue acpi=off"
msgstr ""

#: help.xml:286
msgid ""
"There is no dedicated rescue mode on this CD. However, since the entire "
"system runs from the CD, it is possible to use the command-line and/or "
"graphical tools provided to rescue a broken system, and to use a web browser "
"to search for help. Extensive advice is available online for most kinds of "
"problems that might cause your normal system to fail to boot correctly."
msgstr ""

#: help.xml:308
msgid "SPECIAL BOOT PARAMETERS - OVERVIEW"
msgstr ""

#: help.xml:315
msgid "On a few systems, you may need to specify a parameter at the"
msgstr ""

#: help.xml:316 help.xml:382 help.xml:468 help.xml:535
#, no-wrap
msgid "boot:"
msgstr ""

#: help.xml:316
msgid ""
"prompt in order to boot the system. For example, Linux may not be able to "
"autodetect your hardware, and you may need to explicitly specify its "
"location or type for it to be recognized."
msgstr ""

#: help.xml:322
msgid "For more information about what boot parameters you can use, press:"
msgstr ""

#: help.xml:330
msgid "boot parameters for special machines"
msgstr ""

#: help.xml:335
msgid "boot parameters for various disk controllers"
msgstr ""

#: help.xml:340
msgid "boot parameters understood by the install system"
msgstr ""

#: help.xml:345
msgid "boot parameters understood by the bootstrap system"
msgstr ""

#: help.xml:350
msgid ""
"Many kernel modules are loaded dynamically by the installer, and parameters "
"for those modules cannot be given on the command line. To be prompted for "
"parameters when modules are loaded, boot in expert mode (see <link linkend="
"\"F3\"><keycap>F3</keycap></link>)."
msgstr ""

#: help.xml:357
msgid ""
"Many kernel modules are loaded dynamically by the bootstrap system, and "
"parameters for those modules cannot be given on the command line."
msgstr ""

#: help.xml:375
msgid "SPECIAL BOOT PARAMETERS - VARIOUS HARDWARE"
msgstr ""

#: help.xml:382 help.xml:468 help.xml:535
msgid "You can use the following boot parameters at the"
msgstr ""

#: help.xml:383
msgid ""
"prompt, in combination with the boot method (see <link linkend=\"F3"
"\"><keycap>F3</keycap></link>>). If you use hex numbers you have to use the "
"0x prefix (e.g., 0x300)."
msgstr ""

#: help.xml:390 help.xml:475
msgid "HARDWARE"
msgstr ""

#: help.xml:391 help.xml:476
msgid "PARAMETER TO SPECIFY"
msgstr ""

#: help.xml:394
msgid "IBM PS/1 or ValuePoint (IDE disk)"
msgstr ""

#: help.xml:395
msgid ""
"<userinput>hd=<replaceable>cylinders</replaceable>,<replaceable>heads</"
"replaceable>,<replaceable>sectors</replaceable></userinput>"
msgstr ""

#: help.xml:399
msgid "Some IBM ThinkPads"
msgstr ""

#: help.xml:400
msgid "<userinput>floppy.floppy=thinkpad</userinput>"
msgstr ""

#: help.xml:404
msgid "IBM Pentium Microchannel"
msgstr ""

#: help.xml:405
msgid "<userinput>mca-pentium no-hlt</userinput>"
msgstr ""

#: help.xml:409
msgid "Protect I/O port regions"
msgstr ""

#: help.xml:410
msgid ""
"<userinput>reserve=<replaceable>iobase</replaceable>,<replaceable>extent</"
"replaceable></userinput><optional><userinput>,<replaceable>...</"
"replaceable></userinput></optional>"
msgstr ""

#: help.xml:414
msgid "Workaround faulty FPU (old machines)"
msgstr ""

#: help.xml:415
msgid "<userinput>no387</userinput>"
msgstr ""

#: help.xml:419
msgid "Laptops with screen display problems"
msgstr ""

#: help.xml:420
msgid "<userinput>vga=771</userinput>"
msgstr ""

#: help.xml:425
msgid ""
"If your system hangs when booting, and the last message you see is \"aec671x-"
"detect..\", try"
msgstr ""

#: help.xml:428
msgid "<userinput>gdth=disable:y</userinput>"
msgstr ""

#: help.xml:433
msgid ""
"If you experience lockups or other hardware failures, disable buggy APIC "
"interrupt routing"
msgstr ""

#: help.xml:436
msgid "<userinput>noapic nolapic</userinput>"
msgstr ""

#: help.xml:440 help.xml:509 help.xml:600
msgid "For example:"
msgstr ""

#: help.xml:443
#, no-wrap
msgid "boot: install vga=771 noapic nolapic"
msgstr ""

#: help.xml:444
#, no-wrap
msgid "boot: live vga=771 noapic nolapic"
msgstr ""

#: help.xml:461
msgid "SPECIAL BOOT PARAMETERS - VARIOUS DISK DRIVES"
msgstr ""

#: help.xml:469 help.xml:536
msgid ""
"prompt, in combination with the boot method (see <link linkend=\"F3"
"\"><keycap>F3</keycap></link>)."
msgstr ""

#: help.xml:479
msgid "Adaptec 151x, 152x"
msgstr ""

#: help.xml:480
msgid ""
"<userinput>aha152x.aha152x=<replaceable>iobase</replaceable></"
"userinput><optional><userinput>,<replaceable>irq</replaceable></"
"userinput><optional><userinput>,<replaceable>scsi-id</replaceable></"
"userinput><optional><userinput>,<replaceable>reconnect</replaceable></"
"userinput></optional></optional></optional>"
msgstr ""

#: help.xml:484
msgid "Adaptec 1542"
msgstr ""

#: help.xml:485
msgid ""
"<userinput>aha1542.aha1542=<replaceable>iobase</replaceable></"
"userinput><optional><userinput>,<replaceable>buson</replaceable>,"
"<replaceable>busoff</replaceable></userinput><optional><userinput>,"
"<replaceable>dmaspeed</replaceable></userinput></optional></optional>"
msgstr ""

#: help.xml:489
msgid "Adaptec 274x, 284x"
msgstr ""

#: help.xml:490
msgid "<userinput>aic7xxx.aic7xxx=no_reset</userinput> (enabled if non-zero)"
msgstr ""

#: help.xml:494
msgid "BusLogic SCSI Hosts"
msgstr ""

#: help.xml:495
msgid ""
"<userinput>BusLogic.BusLogic=<replaceable>iobase</replaceable></userinput>"
msgstr ""

#: help.xml:499
msgid "Certain DELL machines"
msgstr ""

#: help.xml:500
msgid "<userinput>aic7xxx.aic7xxx=no_probe</userinput>"
msgstr ""

#: help.xml:505
msgid ""
"This list is incomplete, see the kernel's kernel-parameters.txt file for "
"more."
msgstr ""

#: help.xml:512
#, no-wrap
msgid "boot: install aic7xxx.aic7xxx=no_probe"
msgstr ""

#: help.xml:513
#, no-wrap
msgid "boot: live aic7xxx.aic7xxx=no_probe"
msgstr ""

#: help.xml:530
msgid "SPECIAL BOOT PARAMETERS - INSTALLATION SYSTEM"
msgstr ""

#: help.xml:540
msgid "These parameters control how the installer works."
msgstr ""

#: help.xml:543
msgid "These parameters control how the bootstrap system works."
msgstr ""

#: help.xml:548
msgid "RESULT"
msgstr ""

#: help.xml:549
msgid "PARAMETER"
msgstr ""

#: help.xml:552
msgid "Verbose debugging"
msgstr ""

#: help.xml:553
msgid "<userinput>DEBCONF_DEBUG=5</userinput>"
msgstr ""

#: help.xml:557
msgid "Debug boot sequence"
msgstr ""

#: help.xml:558
msgid "<userinput>BOOT_DEBUG=2|3</userinput>"
msgstr ""

#: help.xml:562
msgid "Disable framebuffer"
msgstr ""

#: help.xml:563
msgid "<userinput>fb=false</userinput>"
msgstr ""

#: help.xml:567
msgid "Don't probe for USB"
msgstr ""

#: help.xml:568
msgid "<userinput>debian-installer/probe/usb=false</userinput>"
msgstr ""

#: help.xml:572
msgid "Don't start PCMCIA"
msgstr ""

#: help.xml:573
msgid "<userinput>hw-detect/start_pcmcia=false</userinput>"
msgstr ""

#: help.xml:577
msgid "Force static network config"
msgstr ""

#: help.xml:578
msgid "<userinput>netcfg/disable_dhcp=true</userinput>"
msgstr ""

#: help.xml:582
msgid "Set keyboard map"
msgstr ""

#: help.xml:583
msgid "<userinput>bootkbd=es</userinput>"
msgstr ""

#: help.xml:587
msgid "Use Braille tty"
msgstr ""

#: help.xml:588
msgid ""
"<userinput>brltty=<replaceable>driver</replaceable>,<replaceable>device</"
"replaceable>,<replaceable>texttable</replaceable></userinput>"
msgstr ""

#: help.xml:593
msgid ""
"Disable ACPI for PCI maps (handy for some HP servers and Via-based machines)"
msgstr ""

#: help.xml:596
msgid "<userinput>pci=noacpi</userinput>"
msgstr ""

#: help.xml:603
#, no-wrap
msgid "boot: install fb=false"
msgstr ""

#: help.xml:604
#, no-wrap
msgid "boot: live fb=false"
msgstr ""

#: help.xml:621
msgid "GETTING HELP"
msgstr ""

#: help.xml:628
msgid "If you can't install Ubuntu, don't despair!"
msgstr ""

#: help.xml:631
msgid "If you can't start Ubuntu, don't despair!"
msgstr ""

#: help.xml:634
msgid "The Ubuntu team is ready to help you!"
msgstr "Ubuntu takımı size yardıma hazır!"

#: help.xml:637
msgid ""
"We are especially interested in hearing about installation problems, because "
"in general they don't happen to only <emphasis>one</emphasis> person."
msgstr ""
"Biz özellikle kurulum problemlerinizi duymak isteriz, çünkü genelde bunlarla "
"sadece <emphasis> bir</emphasis> kişiye olmuyor."

#: help.xml:642
msgid ""
"We are especially interested in hearing about startup problems, because in "
"general they don't happen to only <emphasis>one</emphasis> person."
msgstr ""
"Biz özellikle başlangıç problemlerinizi duymak isteriz, çünkü genelde "
"bunlarla sadece <emphasis> bir</emphasis> kişiye olmuyor."

#: help.xml:646
msgid ""
"We've either already heard about your particular problem and can dispense a "
"quick fix, or we would like to hear about it and work through it with you, "
"and the next user who comes up with the same problem will profit from your "
"experience!"
msgstr ""
"Sizin belirli probleminizi ya daha önce duymuşuzdur ve çabuk bir çözüm "
"sağlayabiliriz ya da sizinle birlikte çözmeye çalışmak isteriz ve aynı "
"problemle karşılaşan bir sonraki kullanıcı ise sizin tecrübenizden "
"yararlanabilir."

#: help.xml:677
msgid "COPYRIGHTS AND WARRANTIES"
msgstr "TELİFHAKLARI VE GARANTİLER"

#: help.xml:684
msgid ""
"Ubuntu is Copyright (C) 2004-2006 Canonical Ltd., and incorporates the work "
"of many other original authors and contributors."
msgstr ""
"Ubuntu'nun telif hakkı Canonical Ltd.'nindir (C) 2004-2006 ve diğer birçok "
"özgün yazarların ve katılımcıların çalışmalarını birleştirir."

#: help.xml:689
msgid "The Ubuntu system is freely redistributable."
msgstr "Ubuntu sistemi serbestçe tekrar dağıtılabilir."

#: help.xml:692
msgid ""
"After installation, the exact distribution terms for each package are "
"described in the corresponding file /usr/share/doc/<replaceable>packagename</"
"replaceable>/copyright."
msgstr ""

#: help.xml:697
msgid ""
"After startup, the exact distribution terms for each package are described "
"in the corresponding file /usr/share/doc/<replaceable>packagename</"
"replaceable>/copyright."
msgstr ""

#: help.xml:703
msgid ""
"Ubuntu comes with <emphasis>ABSOLUTELY NO WARRANTY</emphasis>, to the extent "
"permitted by applicable law."
msgstr ""

#: help.xml:708
msgid ""
"This installation system is based on the Debian installer. See <ulink url="
"\"http://www.debian.org/\" /> for more details and information on the Debian "
"project."
msgstr ""
"Bu kurulum sistemi Debian kurucusu tabanlıdır. Daha fazla detay ve Debian "
"projesi hakkında bilgi için bkz: <ulink url=\"http://www.debian.org/\" />."

#: help.xml:714
msgid ""
"This system is based on Debian. See <ulink url=\"http://www.debian.org/\" /> "
"for more details and information on the Debian project."
msgstr ""
"Bu sistem Debian tabanlıdır. Daha fazla detay ve Debian projesi hakkında "
"bilgi için bkz: <ulink url=\"http://www.debian.org/\" />."
