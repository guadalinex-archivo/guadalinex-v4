# THIS FILE IS AUTOMATICALLY GENERATED FROM THE MASTER FILE
# packages/po/ka.po
#
# DO NOT MODIFY IT DIRECTLY : SUCH CHANGES WILL BE LOST
# 
# Georgian messages for debian-installer.
# Copyright (C) 2003 Software in the Public Interest, Inc.
# This file is distributed under the same license as debian-installer.
#
# Aiet Kolkhi <aietkolkhi@gmail.com>, 2005, 2006.
#
# This file is maintained by Aiet Kolkhi <aietkolkhi@gmail.com>
#
msgid ""
msgstr ""
"Project-Id-Version: debian-installer\n"
"Report-Msgid-Bugs-To: \n"
"POT-Creation-Date: 2006-05-05 20:36+0200\n"
"PO-Revision-Date: 2006-05-26 18:01+0400\n"
"Last-Translator: Aiet Kolkhi <aietkolkhi@gmail.com>\n"
"Language-Team: Debian Georgian <ka@aiet.qartuli.net>\n"
"MIME-Version: 1.0\n"
"Content-Type: text/plain; charset=UTF-8\n"
"Content-Transfer-Encoding: 8bit\n"
"X-Poedit-Bookmarks: 96,-1,-1,-1,-1,-1,-1,-1,-1,-1\n"
"Plural-Forms: nplurals=1; plural=0;\n"
"X-Poedit-Country: GEORGIA\n"
"X-Poedit-SourceCharset: utf-8\n"

#. Type: boolean
#. Description
#: ../netcfg-common.templates:4
msgid "Auto-configure network with DHCP?"
msgstr "გსურთ ქსელის ავტო-კონფიგურაცია DHCP-ს გამოყენებით?"

#. Type: boolean
#. Description
#: ../netcfg-common.templates:4
msgid ""
"Networking can either be configured by DHCP or by manually entering all the "
"information. If you choose to use DHCP and the installer is unable to get a "
"working configuration from a DHCP server on your network, you will be given "
"the opportunity to configure your network manually after the attempt to "
"configure it by DHCP."
msgstr ""
"ქსელის კონფიგურაცია ავტომატურად, DHCP-ს საშუალებითაც შეიძლება და ხელით ყველა "
"მონაცემის მითითებითაც. თუ DHCP მეთოდს ამოირჩევთ და ინსტალაციის პროგრამა "
"თქვენი ქსელის DHCP სერვერიდან მოქმედ კონფიგურაციას ვერ მიიღებს, თქვენ კვლავ "
"შეგეძლებათ ხელით შეიყვანოთ მონაცემები."

#. Type: string
#. Description
#: ../netcfg-common.templates:14
#, fuzzy
msgid "Domain name:"
msgstr "&დომეინის სახელი:"

#. Type: string
#. Description
#: ../netcfg-common.templates:14
msgid ""
"The domain name is the part of your Internet address to the right of your "
"host name.  It is often something that ends in .com, .net, .edu, or .org.  "
"If you are setting up a home network, you can make something up, but make "
"sure you use the same domain name on all your computers."
msgstr ""
"დომენის სახელი თქვენი ინტერნეტის მისამართის ნაწილია, რომელიც სერვერის "
"ნაწილის მარჯვნივ არის მოთავსებული.  ხშირად მისი დაბოლოებებია .com, .net, ."
"edu, ან .org.  თუ თქვენ საშინაო ქსელს აყენებთ, შეგიძლიათ რაიმე დომენი "
"მოიფიქროთ, თუმცა ამ ქსელზე მიერთებულ კომპიუტერებზე ყველგან ეს დომენი უნდა "
"იქნეს მითითებული."

#. Type: string
#. Description
#: ../netcfg-common.templates:22
#, fuzzy
msgid "Name server addresses:"
msgstr "სერვერის სახელი არ არის მითითებული."

#. Type: string
#. Description
#: ../netcfg-common.templates:22
msgid ""
"The name servers are used to look up host names on the network. Please enter "
"the IP addresses (not host names) of up to 3 name servers, separated by "
"spaces. Do not use commas. The first name server in the list will be the "
"first to be queried. If you don't want to use any name server, just leave "
"this field blank."
msgstr ""
"name server-ები ქსელში სერვერის სახელების მოსაძიებლად გამოიყენება. გთხოვთ "
"შეიყვანოთ name server-ეს IP-მისამართები (და არა სერვერის სახელები), მაქს. "
"სამი name server-ის. შენატანი შორისებით გამოყავით. არ გამოიყენოთ მძიმეები. "
"მოთხოვნა პირველად სიაში მყოფი პირველი name server-ს გაეგზავნება. თუ არ გსურთ "
"name server-ის გამოყენება, ეს ველი ცარიელი დატოვეთ."

#. Type: select
#. Description
#: ../netcfg-common.templates:32
#, fuzzy
msgid "Primary network interface:"
msgstr "ინტერფეისის მენიუები"

#. Type: select
#. Description
#: ../netcfg-common.templates:32
msgid ""
"Your system has multiple network interfaces. Choose the one to use as the "
"primary network interface during the installation. If possible, the first "
"connected network interface found has been selected."
msgstr ""
"თქვენ სისტემას ქსელის რამდენიმე ინტერფეისი გააჩნია. ამოირჩიეთ ინსტააციის "
"დროს გამოსაყენებელი მთავარი ქსელის ინტერფეისი. შესაძლებლობის შემთხვევაში, "
"ნაპოვნი ქსელის ინტერფეისებიდან პირველი შეერთებული ინტერფეისი იქნა ამორჩეული."

#. Type: string
#. Description
#: ../netcfg-common.templates:39
msgid ""
"${iface} is a wireless network interface. Please enter the name (the ESSID) "
"of the wireless network you would like ${iface} to use. If you would like to "
"use any available network, leave this field blank."
msgstr ""
"${iface} უკაბელო ქსელის ინტერფეისია. გთხოვთ შეიყვანოთ უკაბელო ქსელის სახელი "
"(ESSID), რომელიც გსურთ ${iface}-მა გამოიყენოს. თუ რომელიმე არსებული ქსელის "
"გამოყენება გსურთ, დატოვეთ ეს ველი ცარიელი."

#. Type: string
#. Description
#: ../netcfg-common.templates:46
msgid "Wireless ESSID for ${iface}:"
msgstr "უკაბელო ESSID: ${iface}:"

#. Type: string
#. Description
#: ../netcfg-common.templates:46
msgid "Attempting to find an available wireless network failed."
msgstr ""

#. Type: string
#. Description
#: ../netcfg-common.templates:46
#, fuzzy
msgid ""
"${iface} is a wireless network interface. Please enter the name (the ESSID) "
"of the wireless network you would like ${iface} to use. To skip wireless "
"configuration and continue, leave this field blank."
msgstr ""
"${iface} უკაბელო ქსელის ინტერფეისია. გთხოვთ შეიყვანოთ უკაბელო ქსელის სახელი "
"(ESSID), რომელიც გსურთ ${iface}-მა გამოიყენოს. თუ რომელიმე არსებული ქსელის "
"გამოყენება გსურთ, დატოვეთ ეს ველი ცარიელი."

#. Type: string
#. Description
#: ../netcfg-common.templates:55
msgid "WEP key for wireless device ${iface}:"
msgstr "უკაბელო მოწყობილობა ${iface}-ის WEP-გასაღები:"

#. Type: string
#. Description
#: ../netcfg-common.templates:55
msgid ""
"If applicable, please enter the WEP security key for the wireless device "
"${iface}. There are two ways to do this:"
msgstr ""
"შესაძლებლობის შემთხვევაში, გთხოვთ შეიყვანოთ WEP უსაფრთხოების გასაღები "
"უკაბელო მოწყობილობა ${iface}-ისთვის. ამისათვის ორი გზა არსებობს:"

#. Type: string
#. Description
#: ../netcfg-common.templates:55
msgid ""
"If your WEP key is in the format 'nnnn-nnnn-nn', 'nn:nn:nn:nn:nn:nn:nn:nn', "
"or 'nnnnnnnn', where n is a number, just enter it as it is into this field."
msgstr ""
"თუ ტქვენ WEP-გასაღები „nnnn-nnnn-nn”, „nn:nn:nn:nn:nn:nn:nn:nn”, ან "
"„nnnnnnnn” ფორმატშია, სადაც n ნომერს აღნიშნავს, უბრალოდ შეიყვანეთ იგი ამ "
"ველში."

#. Type: string
#. Description
#: ../netcfg-common.templates:55
msgid ""
"If your WEP key is in the format of a passphrase, prefix it with "
"'s:' (without quotes)."
msgstr ""
"თუ თქვენი WEP-გასაღები პაროლის ფორმატშია, წაუმძღვარეთ მას „s” (ბრჭყალების "
"გარეშე)."

#. Type: string
#. Description
#: ../netcfg-common.templates:55
msgid ""
"Of course, if there is no WEP key for your wireless network, leave this "
"field blank."
msgstr ""
"რა თქმა უნდა, თუ თქვენი უკაელო ქსელი WEP-გასაღებს არ საჭიროებს, ეს ველი "
"ცარიელი უნდა დატოვოთ."

#. Type: error
#. Description
#: ../netcfg-common.templates:70
#, fuzzy
msgid "Invalid WEP key"
msgstr "არასწორი გადაცემის გასაღები."

#. Type: error
#. Description
#: ../netcfg-common.templates:70
msgid ""
"The WEP key '${wepkey}' is invalid. Please refer to the instructions on the "
"next screen carefully on how to enter your WEP key correctly, and try again."
msgstr ""
"WEP-გასაღები „${wepkey}” არასწორია. გულდასმით გადახედეთ WEP-გასაღების "
"შეყვანის მითითებებს შემდეგ ერკანზე და შეეცადეთ ისევ."

#. Type: error
#. Description
#: ../netcfg-common.templates:77
#, fuzzy
msgid "Invalid ESSID"
msgstr "არასწორი კოდირება %1.\n"

#. Type: error
#. Description
#: ../netcfg-common.templates:77
msgid ""
"The ESSID \"${essid}\" is invalid. ESSIDs may only be up to 32 characters, "
"but may contain all kinds of characters."
msgstr ""
"ESSID „${essid}” არასწორია. ESSID შესაძლოა მაქს. 32 ასონიშნისაგან "
"შედგებოდეს, თუმცა მასში ყველა სახი სიმბოლო შეიძლება შედიოდეს."

#. Type: string
#. Description
#: ../netcfg-common.templates:84
#, fuzzy
msgid "Hostname:"
msgstr "მასპინძელი სერვერის სახელი:"

#. Type: string
#. Description
#: ../netcfg-common.templates:84
#, fuzzy
msgid "Please enter the hostname for this system."
msgstr "გთხოვთ ამ მონიტორის სახელი შეიყვანოთ"

#. Type: string
#. Description
#: ../netcfg-common.templates:84
msgid ""
"The hostname is a single word that identifies your system to the network. If "
"you don't know what your hostname should be, consult your network "
"administrator. If you are setting up your own home network, you can make "
"something up here."
msgstr ""
"სერვერის სახელი, იგივე „hostname” ერთი სიტყვა გახლავთ, რომელიც თქვენი "
"სისტემის სახელია ქსელში. თუ არ იცით რა უნდა იყოს თქვენი „hostname”, "
"შეეკითხეთ ქსელის ადმინისტრატორს. თუ საშინაო ქსელს ქმნით, შეგიძლიათ სახელი "
"თავად მოიგონოთ."

#. Type: error
#. Description
#: ../netcfg-common.templates:94
#, fuzzy
msgid "Invalid hostname"
msgstr "ბაზის ჰოსტის სახელი"

#. Type: error
#. Description
#: ../netcfg-common.templates:94
#, fuzzy
msgid "The name \"${hostname}\" is invalid."
msgstr "სერტიფიკატის დამოწმება არაა სწორე."

#. Type: error
#. Description
#: ../netcfg-common.templates:94
msgid ""
"A valid hostname may contain only the numbers 0-9, the lowercase letters a-"
"z, and the minus sign. It must be between 2 and 63 characters long, and may "
"not begin or end with a minus sign."
msgstr ""
"ვართებული „hostname” შეიძლება მხოლოდ შედგებოდეს ციფრებისაგან (0-9), ქვედა "
"რეესტრის ლათინური ასოებისაგან (a-z) და მინუსის სიმბოლოსაგან. მისი დაშვებული "
"სიგრძეა 2-დან 63 სიმბოლომდე და არ შეიძლება დასრულდეს მინუსის სიმბოლოთი."

#. Type: error
#. Description
#: ../netcfg-common.templates:103
#, fuzzy
msgid "Error"
msgstr "შეცდომა"

#. Type: error
#. Description
#: ../netcfg-common.templates:103
msgid ""
"An error occurred and the network configuration process has been aborted. "
"You may retry it from the installation main menu."
msgstr ""
"შეცდომის გამო ქსელის კონფიგურაციის პროცესი შეწყდა. თქვენ შეგიძლიათ კვლავ "
"შეეცადოთ ინსტალაციის მთავარი მენიუდან."

#. Type: error
#. Description
#: ../netcfg-common.templates:109
#, fuzzy
msgid "No network interfaces detected"
msgstr "PCMCIA კონტროლერი არ აღმოჩნდა"

#. Type: error
#. Description
#: ../netcfg-common.templates:109
msgid ""
"No network interfaces were found. The installation system was unable to find "
"a network device."
msgstr ""
"ქსელის ინტერფეისი ვერ მოიძებნა. ინსტალაციის სისტემამ ქსელის მოწყობილობა ვერ "
"იპოვა."

#. Type: error
#. Description
#: ../netcfg-common.templates:109
msgid ""
"You may need to load a specific module for your network card, if you have "
"one. For this, go back to the network hardware detection step."
msgstr ""
"თუ გაგაჩნიათ, შეგიძლიათ თქვენი ქსელის ინტერფეისისათვის გარკვეული მოდული "
"ჩატვირთოთ. ამისათვის დაუბრუნდით აპარატურის ამოცნობის საფეხურს."

#. Type: note
#. Description
#. A "kill switch" is a physical switch found on some network cards that
#. disables the card.
#: ../netcfg-common.templates:120
msgid "Kill switch enabled on ${iface}"
msgstr "${iface} ბარათზე აქტივირებულია „Kill switch” (გამომრთველი)"

#. Type: note
#. Description
#. A "kill switch" is a physical switch found on some network cards that
#. disables the card.
#: ../netcfg-common.templates:120
msgid ""
"${iface} appears to have been disabled by means of a physical \"kill switch"
"\". If you intend to use this interface, please switch it on before "
"continuing."
msgstr ""
"როგორც ჩანს ${iface} გამორთულია ფიზიკური „kill switch” გამომრთველით. თუ ამ "
"მოწყობილობის გამოყენება გსურთ, გთხოვთ ჩართოთ იგი გაგრძელებამდე."

#. Type: select
#. Choices
#. Note to translators : Please keep your translations of each choice
#. below the 65 columns limit (which means 65 characters for most languages)
#. Choices MUST be separated by commas
#. You MUST use standard commas not special commas for your language
#. You MUST NOT use commas inside choices
#: ../netcfg-common.templates:133
#, fuzzy
msgid "Infrastructure (Managed) network"
msgstr "ქსელური პრობლემა"

#. Type: select
#. Choices
#. Note to translators : Please keep your translations of each choice
#. below the 65 columns limit (which means 65 characters for most languages)
#. Choices MUST be separated by commas
#. You MUST use standard commas not special commas for your language
#. You MUST NOT use commas inside choices
#: ../netcfg-common.templates:133
msgid "Ad-hoc network (Peer to peer)"
msgstr "Ad-hoc ქსელი (Peer to peer)"

#. Type: select
#. Description
#: ../netcfg-common.templates:134
#, fuzzy
msgid "Wireless network kind:"
msgstr "ქსელური პრობლემა"

#. Type: select
#. Description
#: ../netcfg-common.templates:134
msgid ""
"Wireless networks are either managed or ad-hoc. If you use a real access "
"point of some sort, your network is Managed. If another computer is your "
"'access point', then your network may be Ad-hoc."
msgstr ""
"უკაბელო ქსელები ან იმართება, ან „ad-hoc” მდგომარეობაშია. თუ თქვენ რაიმე "
"სახის რეალურ წვდომის წერტილს იყენებთ, თქვენი ქსელი იმართება. თუ თქვენი "
"„წვდომის წერტილი” უბრალოდ სხვა კომპიუტერია, მაშინ შესაძლოა თქვენი ქსელი „Ad-"
"hoc” იყოს."

#. Type: text
#. Description
#: ../netcfg-common.templates:141
#, fuzzy
msgid "Wireless network configuration"
msgstr "კონფიგურაციის დეტალები"

#. Type: text
#. Description
#: ../netcfg-common.templates:145
msgid "Searching for wireless access points ..."
msgstr "მიმდინარეობს უკაბელო ქსელის წვდომის წერტილების ძიება..."

#. Type: text
#. Description
#: ../netcfg-common.templates:155
#, fuzzy
msgid "<none>"
msgstr "არა"

#. Type: text
#. Description
#: ../netcfg-common.templates:159
msgid "Wireless ethernet (802.11x)"
msgstr "უკაბელო ethernet-ქსელი (802.11x)"

#. Type: text
#. Description
#: ../netcfg-common.templates:163
msgid "wireless"
msgstr "უკაბელო"

#. Type: text
#. Description
#: ../netcfg-common.templates:167
#, fuzzy
msgid "Ethernet or Fast Ethernet"
msgstr "წინ გადახვევა"

#. Type: text
#. Description
#: ../netcfg-common.templates:171
#, fuzzy
msgid "Token Ring"
msgstr "შიდა რგოლი 3:"

#. Type: text
#. Description
#: ../netcfg-common.templates:175
#, fuzzy
msgid "USB net"
msgstr "KSim-ის ქსელის მოდული"

#. Type: text
#. Description
#: ../netcfg-common.templates:184
#, fuzzy
msgid "Serial-line IP"
msgstr "ლოკალური &IP მისამართი /"

#. Type: text
#. Description
#: ../netcfg-common.templates:188
#, fuzzy
msgid "Parallel-port IP"
msgstr "ლოკალური &IP მისამართი /"

#. Type: text
#. Description
#: ../netcfg-common.templates:192
#, fuzzy
msgid "Point-to-Point Protocol"
msgstr "უსაფრთხოების პროტოკოლი"

#. Type: text
#. Description
#: ../netcfg-common.templates:196
#, fuzzy
msgid "IPv6-in-IPv4"
msgstr "მოძებნე აქ:"

#. Type: text
#. Description
#: ../netcfg-common.templates:200
msgid "ISDN Point-to-Point Protocol"
msgstr "ISDN Point-to-Point პროტოკოლი"

#. Type: text
#. Description
#: ../netcfg-common.templates:204
#, fuzzy
msgid "Channel-to-channel"
msgstr "დასამატებელი მე&ტსახელი/არხი:"

#. Type: text
#. Description
#: ../netcfg-common.templates:208
#, fuzzy
msgid "Real channel-to-channel"
msgstr "დასამატებელი მე&ტსახელი/არხი:"

#. Type: text
#. Description
#: ../netcfg-common.templates:217
msgid "Inter-user communication vehicle"
msgstr "Inter-user communication vehicle"

#. Type: text
#. Description
#: ../netcfg-common.templates:221
#, fuzzy
msgid "Unknown interface"
msgstr "ქსელი ინტერფეისი"

#. Type: text
#. Description
#. base-installer progress bar item
#: ../netcfg-common.templates:226
#, fuzzy
msgid "Storing network settings ..."
msgstr "ქსელური პროქსის პარამეტრების დაყენება"

#. Type: text
#. Description
#. Item in the main menu to select this package
#: ../netcfg-common.templates:231
#, fuzzy
msgid "Configure the network"
msgstr "ქსელი"

#. Type: string
#. Description
#: ../netcfg-dhcp.templates:3
#, fuzzy
msgid "DHCP hostname:"
msgstr "ბაზის ჰოსტის სახელი"

#. Type: string
#. Description
#: ../netcfg-dhcp.templates:3
msgid ""
"You may need to supply a DHCP host name. If you are using a cable modem, you "
"might need to specify an account number here."
msgstr ""
"თქვენ შესაძლოა DHCP სერვერის სახელის მითითება მოგიწიოთ. თუ თქვენ საკაბელო "
"მოდემს იყენებთ, შესაძლოა აქ ანგარიშის მითითება მოგიწიოთ."

#. Type: string
#. Description
#: ../netcfg-dhcp.templates:3
msgid "Most other users can just leave this blank."
msgstr "უმეტეს სხვა მომხმარებელს შეუძლია ეს ველი ცარიელი დატოვოს."

#. Type: text
#. Description
#: ../netcfg-dhcp.templates:11
#, fuzzy
msgid "Configuring the network with DHCP"
msgstr "ამ ქსელთან ასოცირებული IRC სერვერი"

#. Type: text
#. Description
#: ../netcfg-dhcp.templates:15
msgid "This may take some time."
msgstr "ამას შესაძლოა გარკვეული დრო დაჭირდეს."

#. Type: text
#. Description
#: ../netcfg-dhcp.templates:19
msgid "Network autoconfiguration has succeeded"
msgstr "ქსელის ავტოკონფიგურაცია წარმატენოთ დასრულდა"

#. Type: error
#. Description
#: ../netcfg-dhcp.templates:23
#, fuzzy
msgid "No DHCP client found"
msgstr "პროგრამა ვერ ვიპოვე"

#. Type: error
#. Description
#: ../netcfg-dhcp.templates:23
msgid "No DHCP client was found. This package requires pump or dhcp-client."
msgstr "ნაპოვნია DHCP კლიენტი. ეს პაკეტი pump ან dhcp-კლიენტს საჭიროებს."

#. Type: error
#. Description
#: ../netcfg-dhcp.templates:23
msgid "The DHCP configuration process will be aborted."
msgstr "DHCP კონფიგურაციის პროცესი შეწყდა."

#. Type: select
#. Choices
#. Note to translators : Please keep your translation
#. below a 65 columns limit (which means 65 characters
#. in single-byte languages)
#: ../netcfg-dhcp.templates:33
#, fuzzy
msgid "Retry network autoconfiguration"
msgstr "ქსელური პრობლემა"

#. Type: select
#. Choices
#. Note to translators : Please keep your translation
#. below a 65 columns limit (which means 65 characters
#. in single-byte languages)
#: ../netcfg-dhcp.templates:33
msgid "Retry network autoconfiguration with a DHCP hostname"
msgstr "ქსელის ავტოკონფიგურაციის გამეორება DHCP სერვერის სახელით"

#. Type: select
#. Choices
#. Note to translators : Please keep your translation
#. below a 65 columns limit (which means 65 characters
#. in single-byte languages)
#: ../netcfg-dhcp.templates:33
#, fuzzy
msgid "Configure network manually"
msgstr "ქსელური პრობლემა"

#. Type: select
#. Choices
#. Note to translators : Please keep your translation
#. below a 65 columns limit (which means 65 characters
#. in single-byte languages)
#: ../netcfg-dhcp.templates:33
msgid "${wifireconf}"
msgstr "${wifireconf}"

#. Type: select
#. Choices
#. Note to translators : Please keep your translation
#. below a 65 columns limit (which means 65 characters
#. in single-byte languages)
#: ../netcfg-dhcp.templates:33
msgid "Do not configure the network at this time"
msgstr "ამ ეტაპზე ქსელის კონგიგურაცია არ მოახდინო"

#. Type: select
#. Description
#: ../netcfg-dhcp.templates:35
#, fuzzy
msgid "Network configuration method:"
msgstr "მგრძნობიერობის მეთოდი:"

#. Type: select
#. Description
#: ../netcfg-dhcp.templates:35
msgid ""
"From here you can choose to retry DHCP network autoconfiguration (which may "
"succeed if your DHCP server takes a long time to respond) or to configure "
"the network manually. Some DHCP servers require a DHCP hostname to be sent "
"by the client, so you can also choose to retry DHCP network "
"autoconfiguration with a hostname that you provide."
msgstr ""
"აქ შეგიძლიათ კვალვ შეეცადოთ ქსელის DHCP ავტოკონფიგურაცია (რაც შესაძლოა "
"გამოვიდეს, თუ თქვენი DHCP სერვერი საპასუხოდ დიდ დროს საჭიროებს), ან ხელით "
"მოახდინოთ ქსელის კონფიგურაცია. ზოგიერთი DHCP სერვერი კლიენტის მხრიდან DHCP "
"სერვერის სახელის („hostname”) მითითებას მოითხოვს. შესაბამისად, შეგიძლიაღ "
"DHCP ქსელის ავტოკონფიგურაცია თქვენს მიერ მითითებული სერვერის სახელით "
"შეეცადოთ."

#. Type: note
#. Description
#: ../netcfg-dhcp.templates:45
#, fuzzy
msgid "Network autoconfiguration failed"
msgstr "ჩამოტვირთვა ვერ შესრულდა"

#. Type: note
#. Description
#: ../netcfg-dhcp.templates:45
msgid ""
"Your network is probably not using the DHCP protocol. Alternatively, the "
"DHCP server may be slow or some network hardware is not working properly."
msgstr ""
"სავარაუდოდ თქვენი ქსელი DHCP პროტოკოლს არ იყენებს. ასეე შესაძლებელია, რომ "
"DHCP ძალიან ნელია, ან ქსელის სხვა აპარატურა არ მუშაობს გამართულად."

#. Type: boolean
#. Description
#: ../netcfg-dhcp.templates:51
msgid "Continue without a default route?"
msgstr "გსურთ Default-Route-ის გარეშე გაგრძელება?"

#. Type: boolean
#. Description
#: ../netcfg-dhcp.templates:51
msgid ""
"The network autoconfiguration was successful. However, no default route was "
"set: the system does not know how to communicate with hosts on the Internet. "
"This will make it impossible to continue with the installation unless you "
"have the first installation CD-ROM, a 'Netinst' CD-ROM, or packages "
"available on the local network."
msgstr ""
"ქსელის ავტოკონფიგურაცია წარმატებით განხორციელდა. თუმცა, Default-Route არ "
"დაყენებულა: სისტემამ არ იცის, თუ როგორ დაუკავშირდეს კომპიუტერებს ინტერნეტში. "
"შესაბამისად, ინსტალაცია ვერ გაგრძელდება, თუ არ გაგაჩნიათ ინსტალაციის პირველი "
"CD-ROM, „Netinst” CD-ROM ან ლოკალურ ქსელში ხელმისაწვდომი პაკეტები."

#. Type: boolean
#. Description
#: ../netcfg-dhcp.templates:51
msgid ""
"If you are unsure, you should not continue without a default route: contact "
"your local network administrator about this problem."
msgstr ""
"თუ დარწმუნებული არ ხართ, Default-Route-ის გარეშე არ გააგრძელოთ: პრობლემის "
"გადასაჭრელად დაუკავშირდით თქვენს ლოკალურ ადმინისტრატორს."

#. Type: text
#. Description
#: ../netcfg-dhcp.templates:63
#, fuzzy
msgid "Reconfigure the wireless network"
msgstr "ქსელური პრობლემა"

#. Type: string
#. Description
#: ../netcfg-static.templates:3
msgid "IP address:"
msgstr "IP მისამართი:"

#. Type: string
#. Description
#: ../netcfg-static.templates:3
msgid ""
"The IP address is unique to your computer and consists of four numbers "
"separated by periods.  If you don't know what to use here, consult your "
"network administrator."
msgstr ""
"IP-მისამართი თქვენს კომპიუტერს უნიკალურობას ანიჭებს და იგი წერტილებით "
"გამოყოფილი ოთხი რიცხვისაგან შედგება.  თუ არ იცით თქვენი IP-მისამართი, "
"შეეკითხეთ თქვენი ქსელის ადმინისტრატორს."

#. Type: error
#. Description
#: ../netcfg-static.templates:10
msgid "Malformed IP address"
msgstr "არასწორი IP მისამართი"

#. Type: error
#. Description
#: ../netcfg-static.templates:10
msgid ""
"The IP address you provided is malformed. It should be in the form x.x.x.x "
"where each 'x' is no larger than 255. Please try again."
msgstr ""
"თქვენს მიერ მითითებული IP-მისამართი არასწორი ფორმატისაა. მისი მართებული "
"ფორმაა x.x.x.x სადაც „x” არ უნდა აღემატობოდეს 255-ს. გთხოვთ კვლავ შეეცადოთ."

#. Type: string
#. Description
#: ../netcfg-static.templates:16
#, fuzzy
msgid "Point-to-point address:"
msgstr "საპასუხო მისამართი:"

#. Type: string
#. Description
#: ../netcfg-static.templates:16
msgid ""
"The point-to-point address is used to determine the other endpoint of the "
"point to point network.  Consult your network administrator if you do not "
"know the value.  The point-to-point address should be entered as four "
"numbers separated by periods."
msgstr ""
"point-to-point მისამართი გასაზღვრავს point-to-point ქსელის მეორე endpoint-"
"ს.  დაუკავშირდით ქსელის ადმინისტრატორს, თუ მნიშვნელობა არ იცით.  point-to-"
"point მისამართი ოთხი რიცხვისაგან შედგება, რომლებიც წერტილებით არის "
"გამოყოფილი."

#. Type: string
#. Description
#: ../netcfg-static.templates:25
msgid "Netmask:"
msgstr "Netmask:"

#. Type: string
#. Description
#: ../netcfg-static.templates:25
msgid ""
"The netmask is used to determine which machines are local to your network.  "
"Consult your network administrator if you do not know the value.  The "
"netmask should be entered as four numbers separated by periods."
msgstr ""
"netmask განსაზღვრავს, თუ რომელი კომპიუტერები ჩაითვალოს ლოკალურად თქვენს "
"ქსელში.  თუ არ იცით იგი, დაუკევშირდით ქსელის ადმინისტრატორს.  netmask "
"შედგება ოთხი რიცხვისაგან, რომლებიც წერტილებით არის გამოყოფილი."

#. Type: string
#. Description
#: ../netcfg-static.templates:33
msgid "Gateway:"
msgstr "გეითვეი:"

#. Type: string
#. Description
#: ../netcfg-static.templates:33
msgid ""
"The gateway is an IP address (four numbers separated by periods) that "
"indicates the gateway router, also known as the default router.  All traffic "
"that goes outside your LAN (for instance, to the Internet) is sent through "
"this router.  In rare circumstances, you may have no router; in that case, "
"you can leave this blank.  If you don't know the proper answer to this "
"question, consult your network administrator."
msgstr ""
"გეითვეი არის IP-მისამართი (წერტილებით გამოყოფილი ოთხი რიცხვი), რომელიც "
"გეითვეის-როუტერს (იგივე „default router”) განსაზღვრავს.  ყველა ის კავშირი, "
"რომელიც თქვენი ლოკალური ქსელის ფარგლებს გარეთ ხორციელდება (მაგ. ინტერნეტში), "
"ამ როუტერის საშუალებით იგზავნება.  იშვიათ შემთხვევაში, შესაძლოა როუტერი არ "
"გქონდეთ; ასეთ შემთხვევაში, ნურაფერს შეიყვანთ.  თუ ამ შეკითხვაზე სწორი პასუხი "
"არ იცით, დაუკავშირდით ქსელის ადმინისტრატორს."

#. Type: error
#. Description
#: ../netcfg-static.templates:44
msgid "Unreachable gateway"
msgstr "მიუწვდომელი გეითვეი"

#. Type: error
#. Description
#: ../netcfg-static.templates:44
#, fuzzy
msgid "The gateway address you entered is unreachable."
msgstr "თქვენს მიერ შეყვანილი ელფოსტის მისამართი არასწორია"

#. Type: error
#. Description
#: ../netcfg-static.templates:44
msgid ""
"You may have made an error entering your IP address, netmask and/or gateway."
msgstr "შესაძლოა IP-მისამართი, netmask და/ან გეითვეი შეცდომით შეიყვანეთ."

#. Type: boolean
#. Description
#: ../netcfg-static.templates:53
#, fuzzy
msgid "Is this information correct?"
msgstr "ინფორმაცია არ არის სრული."

#. Type: boolean
#. Description
#: ../netcfg-static.templates:53
msgid "Currently configured network parameters:"
msgstr "ქსელის  ამჟამად დაყენებული პარამეტრები:"

#. Type: boolean
#. Description
#: ../netcfg-static.templates:53
msgid ""
" interface     = ${interface}\n"
" ipaddress     = ${ipaddress}\n"
" netmask       = ${netmask}\n"
" gateway       = ${gateway}\n"
" pointopoint   = ${pointopoint}\n"
" nameservers   = ${nameservers}"
msgstr ""
" ინტერფეისი    = ${interface}\n"
" IP-მისამართი  = ${ipaddress}\n"
" Netmask       = ${netmask}\n"
" გეითვეი       = ${gateway}\n"
" Pointopoint   = ${pointopoint}\n"
" Nameservers   = ${nameservers}"

#. Type: text
#. Description
#. Item in the main menu to select this package
#: ../netcfg-static.templates:66
msgid "Configure a network using static addressing"
msgstr "ქსელის სტატიკური ადრესაციით კონფიგურირება"
