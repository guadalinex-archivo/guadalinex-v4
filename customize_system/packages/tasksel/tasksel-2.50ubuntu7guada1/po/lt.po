# translation of tasksel.po to Lithuanian
# This file is distributed under the same license as the tasksel package.
# Copyright (C) 2004, 2005 Free Software Foundation, Inc.
# Kęstutis Biliūnas <kebil@kaunas.init.lt>, 2004, 2005.
#
msgid ""
msgstr ""
"Project-Id-Version: tasksel program\n"
"Report-Msgid-Bugs-To: \n"
"POT-Creation-Date: 2006-01-18 23:04+0900\n"
"PO-Revision-Date: 2005-12-27 00:04+0200\n"
"Last-Translator: Kęstutis Biliūnas <kebil@kaunas.init.lt>\n"
"Language-Team: Lithuanian <komp_lt@konferencijos.lt>\n"
"MIME-Version: 1.0\n"
"Content-Type: text/plain; charset=UTF-8\n"
"Content-Transfer-Encoding: 8bit\n"

#: ../tasksel.pl:358
msgid ""
"Usage:\n"
"tasksel install <task>\n"
"tasksel remove <task>\n"
"tasksel [options]\n"
"\t-t, --test          test mode; don't really do anything\n"
"\t    --new-install   automatically install some tasks\n"
"\t    --list-tasks    list tasks that would be displayed and exit\n"
"\t    --task-packages list available packages in a task\n"
"\t    --task-desc     returns the description of a task\n"
msgstr ""
"Naudojimas:\n"
"tasksel install <uždavinys>\n"
"tasksel remove <uždavinys>\n"
"tasksel [parametrai]\n"
"\t-t, --test          testinis režimas; iš tikrųjų nedaro nieko\n"
"\t    --new-install   automatiškai įdiegia kai kuriuos uždavinius\n"
"\t    --list-tasks    parodo uždavinių sąrašą ir išeina\n"
"\t    --task-packages pateikia uždavinyje esamų paketų sąrašą\n"
"\t    --task-desc     gražina uždavinio aprašymą\n"

#: ../tasksel.pl:557 ../tasksel.pl:575 ../tasksel.pl:586
msgid "aptitude failed"
msgstr "aptitude vykdymas nepavyko"
